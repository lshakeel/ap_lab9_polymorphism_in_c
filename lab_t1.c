#include <stdio.h>
#include <stdlib.h>
#include <math.h>

struct Matrix{
int rows;
int columns;
void (*multiply_ptr)(struct Matrix *, struct Matrix *, struct Matrix *);
void (*add_ptr)(struct Matrix *,struct Matrix *, struct Matrix *);
void (*norm_ptr)(struct Matrix *);
int *array;
} Matrix;

struct Vector{
int max_elem;
int min_elem;
struct Matrix m;
/*
void (*multiply_ptr)(struct Matrix *, struct Matrix *, struct Matrix *);
void (*add_ptr)(struct Matrix *,struct Matrix *, struct Matrix *);
void (*norm_ptr)(struct Matrix *);
int *array;*/
} Vector;



void multiply(struct Matrix * m1, struct Matrix * m2, struct Matrix * res) {
  if(m1->columns != m2->rows)
    {
      printf("Multiplication is not allowed\n");
    }
  else
    {

      int a,b,c;
      for(int i=0;i<m1->rows;i++)
	{
	  for(int k=0; k<m2->columns ; k++)
	    {
	      a= i*m1->columns;
	       b=0;
	   for(int j=0;j<m2->rows;j++)
	     {
	       b+=(m1->array[a+j] * m2->array[j*(m2->columns) +k]);
	       
	     }
	   res->array[i* res->columns +k]=b;
		
        printf("mul[%d]= %d\n", i* res->columns +k,res->array[i* res->columns +k] );
	    }
	}



    }
}
void add(struct Matrix * m1, struct Matrix * m2, struct Matrix * res) {

    for(int i=0; i< m1->rows * m1->columns; i++)
    {
        res->array[i]=m1->array[i] + m2->array[i];
        printf("add[%d]= %d\n", i,res->array[i] );
    }
}

void add_v(struct Matrix * m1, struct Matrix * m2, struct Matrix * res) {
//printf("blah:%d", (res->array[0]));

    for(int i=0; i< m1->rows; i++)
    {
        res->array[i]=m1->array[i]+ m2->array[i];
        printf("add[%d]= %d\n", i,res->array[i] );
    }
}

void norm(struct Matrix * m1) {

    int *n = (int *)malloc(sizeof(int)*m1->columns);
    for(int j=0; j< m1->columns; j++)
    {

	for(int i=0; i< m1->rows; i++)
    	{
        	n[j] += abs((float)m1->array[i]);
	}
    }

    //n= sqrt(n);
    
int maximum=0;
    for (int c = 0; c <m1->columns ; c++)
  {
    if (n[c] > maximum)
    {
       maximum  = n[c];
    }
  }

    printf("norm: %d\n", maximum);

}

void norm_v(struct Matrix * m1) {

    float n = 0;
    for(int i=0; i< m1->rows * m1->columns; i++)
    {
        n += abs((float)m1->array[i]);
    }

    printf("norm_v: %f\n", n);

}

void init_m(struct Matrix * m1, int rows, int columns){
    m1->rows=rows;
    m1->columns= columns;
    m1->multiply_ptr = &multiply;
    m1->add_ptr = &add;
    m1->norm_ptr = &norm;
    m1->array = (int *) malloc(sizeof(int) * rows * rows);
    for(int i=0; i< m1->rows * m1->columns; i++)
    {
        m1->array[i]=2;
       
        printf("init_m[%d]= %d\n", i,m1->array[i] );
    }
}

void init_v(struct Vector * v1, int rows){
    v1->m.rows=rows;
    v1->m.columns= 1;
    v1->m.array = (int *) malloc(sizeof(int) * rows * 1);
    v1->m.multiply_ptr = &multiply;
    v1->m.add_ptr = &add_v;
    v1->m.norm_ptr = &norm_v;
    
    for(int i=0; i< v1->m.rows; i++)
    {
        v1->m.array[i]=2;
       
        printf("init_v[%d]= %d\n", i,v1->m.array[i] );
    }
}


int main(){
    struct Matrix m1;
    struct Matrix m2;
    struct Matrix res;
    init_m(&m1, 2,2);
    init_m(&m2,2,2);
    init_m(&res,2,2);
    m1.add_ptr(&m1, &m2, &res);
    m1.multiply_ptr(&m1, &m2, &res);
    m1.norm_ptr(&m1);

    struct Vector v1;
    init_v(&v1, 2);

    struct Vector v2;
    init_v(&v2, 2);

    struct Vector res_v;
    init_v(&res_v, 2);

    v1.m.add_ptr(&v1.m, &v2.m, &res_v.m);

    v1.m.multiply_ptr(&m2, &v1.m, &res);

    v1.m.norm_ptr(&v1.m);
   
    return 0;
}
